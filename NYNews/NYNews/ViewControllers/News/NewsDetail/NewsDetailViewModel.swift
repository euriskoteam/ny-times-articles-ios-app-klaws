//
//  NewsDetailViewModel.swift
//  NYNews
//
//  Created by Klaws Achkar on 2/18/19.
//  Copyright © 2019 Klaws Achkar. All rights reserved.
//

import Foundation

class NewsDetailViewModel {
    
    var newsDetail: NewsDetail!
    
    init(_ newsDetail: NewsDetail) {
        self.newsDetail = newsDetail
    }
    
}
