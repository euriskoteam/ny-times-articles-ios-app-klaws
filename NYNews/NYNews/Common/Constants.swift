//
//  Constants.swift
//  NYNews
//
//  Created by Klaws Achkar on 2/18/19.
//  Copyright © 2019 Klaws Achkar. All rights reserved.
//

import Foundation

class Constants {
    
    // The Base URL of the Api calls
    static let baseUrl = "http://api.nytimes.com/"
    
    // Most Popular News Api
    // @Params: 1, 7, 30 represents how far back, in days, the API returns results for
    static let getMostPopularNewsApi = "svc/mostpopular/v2/mostviewed/all-sections/%i.json"
    
    // Api Key parameter
    static let paramsApiKey = "api-key"
    
    // Api Key for the NY Times site
    static let apiKey = "yB0GIwEXPrwsRC2ov42Xrzl7mj4xKA41"
    
    public struct Strings {
        //Common strings
        static let MessageTitleError = "Error"
        static let MessageActionOk = "OK"
        static let MessageGenericError = "An unknow error occured, Please try again later!"
        static let MessageNoData = "No news to display, Please try again later!"
        
        // NY News strings
        static let NYNewsPageTitle = "NY News"
        static let NYNewsRefreshMessage = "Refreshing News..."
        
        // News Detail strings
        static let NewsDetailPageTitle = "News Detail"
        static let NewsDetailNbViews = "%i Views"
        
        
    }
}
